//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SRC_VEINS_MODULES_APPLICATION_TRACI_MYRSU_H_
#define SRC_VEINS_MODULES_APPLICATION_TRACI_MYRSU_H_

#include <veins/modules/application/ieee80211p/BaseWaveApplLayer.h>
#include <omnetpp.h>
#include <string.h>
#include <math.h>

using namespace std;

class MyRSU: public BaseWaveApplLayer {

    public:
        int RSUs_assign_target;
        int wsm_receive_count_by_rsu = 0;
        int max_request_distance = 500;
        double distance_temp = 10000;
        double x_temp;
        double y_temp;
        Coord fushin;
        bool is_in = 1;
        string rsu_name;
        const char *rsu_name_c;
        set<int>::iterator ite_s;

        cHistogram report_statistics;

        cOutVector trackerVector;
    protected:
        virtual void initialize(int stage);
        virtual void onWSM(WaveShortMessage* wsm);
        virtual void onWSA(WaveServiceAdvertisment* wsa);
        virtual void handleSelfMsg(cMessage* msg);
        virtual void finish();
};

class my_rsu_class
{
    protected:
        int report_wsm_count;
        bool receive_first_report;
        simtime_t timestamp;
        simtime_t report_timer;
        Coord target_pos;
        set<simtime_t> wsm_set;
        list<int> wsm_list;     /*Τ‘θΆxr*/

    public:
        int getReport_wsm_count() const
        {
            return this->report_wsm_count;
        }
        void setReport_wsm_count(int report_wsm_count)
        {
            this->report_wsm_count = report_wsm_count;
        }

        bool getReceive_first_report() const
        {
            return this->receive_first_report;
        }
        void setReceive_first_report(bool receive_first_report)
        {
            this->receive_first_report = receive_first_report;
        }

        simtime_t getTimestamp() const
        {
            return this->timestamp;
        }
        void setTimestamp(simtime_t timestamp)
        {
            this->timestamp = timestamp;
        }

        simtime_t getReport_timer() const
        {
            return this->report_timer;
        }
        void setReport_timer(simtime_t report_timer)
        {
            this->report_timer = report_timer;
        }

        Coord getTarget_pos() const
        {
            return this->target_pos;
        }
        void setTarget_pos(Coord target_pos)
        {
            this->target_pos = target_pos;
        }

        list<int> getWsm_list()
        {
            return this->wsm_list;
        }
        void setWsm_list(int wsm_number)
        {
            wsm_list.push_front(wsm_number);
        }

        int getWsm_set()
        {
            return this->wsm_set.size();
        }
        bool getWsm_set_empty()
        {
            return this->wsm_set.empty();
        }
        set<simtime_t> getWsm_set_set()
        {
            return this->wsm_set;
        }
        void setWsm_set(simtime_t wsm_timestamp)
        {
            wsm_set.insert(wsm_timestamp);
        }
};



#endif /* SRC_VEINS_MODULES_APPLICATION_TRACI_MYRSU_H_ */
