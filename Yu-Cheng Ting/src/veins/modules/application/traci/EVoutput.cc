//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/modules/application/traci/EVoutput.h>
#include "veins/modules/messages/WaveShortMessage_m.h"
#include "veins/modules/mobility/traci/TraCIScenarioManager.h"
#include "veins/modules/mobility/traci/TraCICommandInterface.h"
#include "veins/modules/mobility/traci/TraCIConnection.h"
#include <string>
#include <iostream>

using namespace std;

Define_Module(EVoutput)

my_rsu_class_t* my_rsu_class_t00 = new my_rsu_class_t;

void EVoutput::initialize(int stage)
{
    BaseWaveApplLayer::initialize(stage);

    if (stage == 0)
    {
        if (getParentModule()->getIndex() == 0)
        {
            list<string>::iterator it;
            it = traci->getJunctionIds().begin();
            string id_x = traci->getJunctionIds().front();

            EV << "stage = 0 " << endl;
            EV << "Output testing: " << endl;

            ev_list.push_front(0);
            ev_list.push_front(1);
            for (ev_it = ev_list.begin(); ev_it != ev_list.end(); ev_it++)
            {
                EV << *ev_it << endl;
            }
            EV << "test over" << endl;

            my_rsu_class_t00->setWsm_list(0);
            my_rsu_class_t00->setWsm_list(1);
            for (ev_it = my_rsu_class_t00->getWsm_list().begin(); ev_it != my_rsu_class_t00->getWsm_list().end(); ev_it++)
            {
                EV << *ev_it << endl;
            }
        }
        else if (getParentModule()->getIndex() == 1)
        {
            EV << "test for another car." << endl;
            for (ev_it = my_rsu_class_t00->getWsm_list().begin(); ev_it != my_rsu_class_t00->getWsm_list().end(); ev_it++)
            {
                EV << *ev_it << endl;
            }
            EV << "The size of list is " << my_rsu_class_t00->getWsm_list().size() << endl;
        }
    }
    else if (stage == 1)
    {
        EV << "stage = 1" << endl;

    }
    else if (stage == 3)
    {
        EV << "stage = 3" << endl;
    }
}

void EVoutput::finish()
{

}

void EVoutput::onBSM(BasicSafetyMessage* bsm)
{

}

void EVoutput::handleSelfMsg(cMessage* msg)
{
    if (WaveShortMessage* wsm = dynamic_cast<WaveShortMessage*>(msg))
    {

    }
    else {
        BaseWaveApplLayer::handleSelfMsg(msg);
    }


}



void EVoutput::onWSM(WaveShortMessage* wsm)
{

}

void EVoutput::handlePositionUpdate(cObject* obj)
{
    BaseWaveApplLayer::handlePositionUpdate(obj);

}
