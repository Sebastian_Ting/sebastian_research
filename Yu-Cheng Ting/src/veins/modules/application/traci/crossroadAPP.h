//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SRC_VEINS_MODULES_APPLICATION_TRACI_CROSSROADAPP_H_
#define SRC_VEINS_MODULES_APPLICATION_TRACI_CROSSROADAPP_H_

#include <veins/modules/application/ieee80211p/BaseWaveApplLayer.h>
#include "veins/modules/application/ieee80211p/BaseWaveApplLayer.h"
#include "veins/modules/mobility/traci/TraCICommandInterface.h"
#include "veins/modules/mobility/traci/TraCIColor.h"
#include "veins/base/utils/Coord.h"
#include "veins/modules/world/traci/trafficLight/TraCITrafficLightProgram.h"
#include "veins/modules/mac/ieee80211p/Mac1609_4.h"
#include <string>
#include <iostream>

using namespace omnetpp;
using namespace Veins;
using namespace std;

class crossroadAPP: public BaseWaveApplLayer {
    public:
        virtual void initialize(int stage);
        virtual void finish();

        bool sent_msg = 0;
        string my_road_id;
        char const *c;
        char const *d;
        list<string> listJ;
        list<string>::iterator list_j;
        string r_id;
        string crossroad_junction = "_1";
        string::size_type idx;
        Coord p1;

    private:
        TraCIScenarioManager* manager;
        TraCIConnection* myconnection;
        TraCICoordinateTransformation* t_coordinate;

    protected:
        virtual void onBSM(BasicSafetyMessage* bsm);
        virtual void onWSM(WaveShortMessage* wsm);
        virtual void handleSelfMsg(cMessage* msg);
        virtual void handlePositionUpdate(cObject* obj);
};

class setup_crossroad: public BaseWaveApplLayer
{
    public:
        void f_01()
        {

        }

};

#endif /* SRC_VEINS_MODULES_APPLICATION_TRACI_CROSSROADAPP_H_ */
