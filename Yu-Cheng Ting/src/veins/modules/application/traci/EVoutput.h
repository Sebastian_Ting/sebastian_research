//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SRC_VEINS_MODULES_APPLICATION_TRACI_EVoutput_H_
#define SRC_VEINS_MODULES_APPLICATION_TRACI_EVoutput_H_

#include <veins/modules/application/ieee80211p/BaseWaveApplLayer.h>
#include "veins/modules/application/ieee80211p/BaseWaveApplLayer.h"
#include "veins/modules/mobility/traci/TraCICommandInterface.h"
#include "veins/modules/mobility/traci/TraCIColor.h"
#include "veins/base/utils/Coord.h"
#include "veins/modules/world/traci/trafficLight/TraCITrafficLightProgram.h"
#include "veins/modules/mac/ieee80211p/Mac1609_4.h"
#include <string>
#include <string.h>
#include <iostream>
#include <stdlib.h>
#include "veins/base/utils/Move.h"
#include "veins/modules/mobility/traci/TraCIConnection.h"
#include "veins/modules/mobility/traci/TraCICoord.h"
#include <veins/modules/mobility/traci/TraCICoordinateTransformation.h>

using namespace omnetpp;
using namespace Veins;
using Veins::TraCIMobilityAccess;
using namespace std;

class EVoutput: public BaseWaveApplLayer {
    public:
        virtual void initialize(int stage);
        virtual void finish();

        list<int> ev_list;
        list<int>::iterator ev_it;

    private:
        TraCIScenarioManager* manager;
        TraCIConnection* myconnection;
        TraCICoordinateTransformation* t_coordinate;
        TraCIConnection* traci_connection;
        TraCICoordinateTransformation* coord_trans;

    protected:
        virtual void onBSM(BasicSafetyMessage* bsm);
        virtual void onWSM(WaveShortMessage* wsm);
        virtual void handleSelfMsg(cMessage* msg);
        virtual void handlePositionUpdate(cObject* obj);
};

class my_rsu_class_t
{
    protected:
        int report_wsm_count;
        list<int> wsm_list;

    public:
        int getReport_wsm_count() const
        {
            return this->report_wsm_count;
        }
        void setReport_wsm_count(int report_wsm_count)
        {
            this->report_wsm_count = report_wsm_count;
        }

        list<int> getWsm_list()
        {
            return this->wsm_list;
        }
        void setWsm_list(int wsm_number)
        {
            wsm_list.push_front(wsm_number);
        }

};

#endif /* SRC_VEINS_MODULES_APPLICATION_TRACI_EVoutput_H_ */
