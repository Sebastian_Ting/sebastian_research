//
// Copyright (C) 2016 David Eckhoff <david.eckhoff@fau.de>
//
// Documentation for these modules is at http://veins.car2x.org/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef __VEINS_MYVEINSAPP_H_
#define __VEINS_MYVEINSAPP_H_

#include <omnetpp.h>
#include "veins/modules/application/ieee80211p/BaseWaveApplLayer.h"
#include "veins/modules/mobility/traci/TraCICommandInterface.h"
#include "veins/modules/mobility/traci/TraCIColor.h"
#include "veins/base/utils/Coord.h"
#include "veins/modules/world/traci/trafficLight/TraCITrafficLightProgram.h"

#include "veins/modules/mac/ieee80211p/Mac1609_4.h"

using namespace omnetpp;
using namespace Veins;

/**
 * @brief
 * This is a stub for a typical Veins application layer.
 * Most common functions are overloaded.
 * See MyVeinsApp.cc for hints
 *
 * @author David Eckhoff
 *
 */

class MyVeinsApp : public BaseWaveApplLayer {
    public:
        virtual void initialize(int stage);
        virtual void finish();

        void tracking_status();
        double distance_with_target();
        void vehicle_setup(int MyCarID);
        void TargetPositionUpdate(int MyCarID);
        void send_Handoff();
        void send_handoffWSM();
        void send_ReportWSM();
        bool SuccessToHandoff(bool sendHandoffMessage);

        bool am_i_a_tracker = 0;
        int delay_time = 4;
        int lost_count_by_node = 0;
        int tracker_index;
        int target_index = 552;
        double MAX_tracking_distance = 15;  /*大於多少距離時發出換手訊息？*/
        double distance_with_sender;
        simtime_t b_at_i;
        simtime_t forward_delay;

        /*圖表及數據統計*/
        int count_of_handoffmessage = 0;
        int total_handoffmessage_count = 0;
        int report_count_per_node = 0;

        cHistogram handoff_statistics;
        cHistogram report_count;
        cOutVector message_count_vector;
        cOutVector tracking_bool;

    private:
        TraCIScenarioManager* manager;
        TraCIConnection* myconnection;

    protected:
        virtual void onBSM(BasicSafetyMessage* bsm);
        virtual void onWSM(WaveShortMessage* wsm);
        virtual void onWSA(WaveServiceAdvertisment* wsa);

        virtual void handleSelfMsg(cMessage* msg);
        virtual void handlePositionUpdate(cObject* obj);

    };

class setup
{
    protected:
        bool replyHandoff;
        bool sendHandoffMessage;
        bool alreadysentAssignWSM;
        bool rsu_receive_lost_message;
        bool we_lost_target;
        int TrackerID;
        int TargetID;
        double saveDsitance;
        int f_count_of_handoffmessage;
        int request_count;
        int serial_number;
        int f_count_of_report;
        int f_count_of_rsu_reply;
        int f_unique_report;
        int handoff_times;
        int corssroad_count;
        int report_array[50] = { };
        int f_report_index(int i);
        int lost_count_in_structure;
        set<int> trackers_index;

    public:
        bool getReplyHandoff() const
        {
            return this->replyHandoff;
        }
        void setReplyHandoff(bool replyHandoff)
        {
            this->replyHandoff = replyHandoff;
        }

        bool getSendHandoffMessage() const
        {
            return this->sendHandoffMessage;
        }
        void setSendHandoffMessage(bool sendHandoffMessage)
        {
            this->sendHandoffMessage = sendHandoffMessage;
        }

        bool getAlreadysentAssignWSM()const
        {
            return this->alreadysentAssignWSM;
        }
        void setAlreadysentAssignWSM(bool alreadysentAssignWSM)
        {
            this->alreadysentAssignWSM = alreadysentAssignWSM;
        }

        bool getRsu_receive_lost_message()
        {
            return this->rsu_receive_lost_message;
        }
        void setRsu_receive_lost_message(bool rsu_receive_lost_message)
        {
            this->rsu_receive_lost_message = rsu_receive_lost_message;
        }

        bool getWe_lost_target()
        {
            return this->we_lost_target;
        }
        void setWe_lost_target(bool we_lost_target)
        {
            this->we_lost_target = we_lost_target;
        }

        int getTrackerID() const
        {
            return this->TrackerID;
        }
        void setTrackerID(int TrackerID)
        {
            this->TrackerID = TrackerID;
        }

        int getTargetID() const
        {
            return this->TargetID;
        }
        void setTargetID(int TargetID)
        {
            this->TargetID = TargetID;
        }

        double getSaveDistance() const
        {
            return this->saveDsitance;
        }
        void setSaveDistance(double saveDistance)
        {
            this->saveDsitance = saveDistance;
        }

        int getF_count_of_handoffmessage() const
        {
            return this->f_count_of_handoffmessage;
        }
        void setF_count_of_handoffmessage(int f_count_of_handoffmessage)
        {
            this->f_count_of_handoffmessage = f_count_of_handoffmessage;
        }

        int getRequest_count() const
        {
            return this->request_count;
        }
        void setRequest_count(int request_count)
        {
            this->request_count = request_count;
        }

        int getSerial_number() const
        {
            return this->serial_number;
        }
        void setSerial_number(int serial_number)
        {
            this->serial_number = serial_number;
        }

        int getF_count_of_report() const
        {
            return this->f_count_of_report;
        }
        void setF_count_of_report(int f_count_of_report)
        {
            this->f_count_of_report = f_count_of_report;
        }

        int getF_count_of_rsu_reply() const
        {
            return this->f_count_of_rsu_reply;
        }
        void setF_count_of_rsu_reply(int f_count_of_rsu_reply)
        {
            this->f_count_of_rsu_reply = f_count_of_rsu_reply;
        }

        int getF_unique_report() const
        {
            return this->f_unique_report;
        }
        void setF_unique_report(int f_unique_report)
        {
            this->f_unique_report = f_unique_report;
        }

        int getHandoff_times() const
        {
            return this->handoff_times;
        }
        void setHandoff_times(int handoff_times)
        {
            this->handoff_times = handoff_times;
        }

        int getCrossroad_count() const
        {
            return this->corssroad_count;
        }
        void setCrossroad_count(int crossroad_count)
        {
            this->corssroad_count = crossroad_count;
        }

        int getF_report_index(int r)
        {
            return this->report_array[r];
        }
        void setF_report_index(int r, int input)
        {
            this->report_array[r] = input;
        }

        int getLost_count_in_structure() const
        {
            return this->lost_count_in_structure;
        }
        void setLost_count_in_structure(int lost_count_in_structure)
        {
            this->lost_count_in_structure = lost_count_in_structure;
        }

        int getTrackers_index_empty()
        {
            return this->trackers_index.empty();
        }
        void insertTrackers_index(int v_node_index)
        {
            trackers_index.insert(v_node_index);
        }
        void removeTrackers_index(int v_node_index)
        {
            trackers_index.erase(v_node_index);
        }
};

#endif
