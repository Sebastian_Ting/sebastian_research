//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/modules/application/traci/MyRSU.h>
#include <stdio.h>
#include <omnetpp.h>

Define_Module(MyRSU);

my_rsu_class* my_rsu_class_00 = new my_rsu_class;

void MyRSU::initialize(int stage)
{
    BaseWaveApplLayer::initialize(stage);
    if (stage == 0)
    {
        rsu_name = "rsu[" + to_string(getParentModule()->getIndex()) + "].mobility";
        rsu_name_c = rsu_name.c_str();

        fushin.x = 5000;
        fushin.y = 5000;
        fushin.z = 3;
        my_rsu_class_00->setTarget_pos(fushin);
        my_rsu_class_00->setReport_wsm_count(0);
        my_rsu_class_00->setReport_timer(0);
        my_rsu_class_00->setReceive_first_report(0);

        trackerVector.setName("tracking_Now");

    }
}

void MyRSU::finish()
{
    recordScalar("#reportWSM_count_by_rsu", wsm_receive_count_by_rsu);
    recordScalar("#reportWSM_count_in_structure", my_rsu_class_00->getReport_wsm_count());
    recordScalar("#message_count_in_reportWSM_set", my_rsu_class_00->getWsm_set());
}

void MyRSU::handleSelfMsg(cMessage* msg)
{
    if (WaveShortMessage* wsm = dynamic_cast<WaveShortMessage*>(msg))       /*來自應用層的訊息*/
    {
        wsm->setSerial(wsm->getSerial() +1);
        if (wsm->getSerial() >= 3) {        /*Serial會影響到要傳送幾次！*/
            //stop service advertisements
            stopService();
            delete(wsm);
        }
        else {
            scheduleAt(simTime() + 10, wsm);
        }
    }
    else    /*信標訊息*/
    {
        if (my_rsu_class_00->getReceive_first_report() == 0)
        {
            msg->setKind(3);
            trackerVector.record(0);
        }
        else if (my_rsu_class_00->getReport_timer() + 5 <= simTime())       /*已超過T秒未收到通報，則位於定義範圍內的RSU將重新發起廣播*/
        {
            trackerVector.record(0);
            EV << "The report timer is " << my_rsu_class_00->getReport_timer() << endl;
            EV << "RSUs guess Target's position is " << my_rsu_class_00->getTarget_pos() << endl;
            if (my_rsu_class_00->getTarget_pos().distance(check_and_cast<BaseMobility*>(getSimulation()->getModuleByPath(rsu_name_c))->getCurrentPosition()) < max_request_distance)
            {
                EV  << rsu_name << ": My position is " << check_and_cast<BaseMobility*>(getSimulation()->getModuleByPath(rsu_name_c))->getCurrentPosition() << endl;
                EV << "This RSU is in broadcast range. Because we guess the target is now at " << my_rsu_class_00->getTarget_pos() << endl;
                EV << "The distance is from " << rsu_name << "to the target is" << my_rsu_class_00->getTarget_pos().distance(check_and_cast<BaseMobility*>(getSimulation()->getModuleByPath(rsu_name_c))->getCurrentPosition()) << endl;
                msg->setKind(3);

            }
        }

        else
        {
            msg->setKind(0);
            trackerVector.record(2);
        }
        BaseWaveApplLayer::handleSelfMsg(msg);
    }
}

void MyRSU::onWSM(WaveShortMessage* wsm)
{
   if (wsm->getPsid() == 5)     /*果我的位置減掉最後收到的位置小於一定距離的話，我的beacon就要附加追蹤要求效果*/
   {
       my_rsu_class_00->setReceive_first_report(1);
       wsm_receive_count_by_rsu = wsm_receive_count_by_rsu + 1;
       my_rsu_class_00->setReport_wsm_count(my_rsu_class_00->getReport_wsm_count() + 1);

       is_in = my_rsu_class_00->getWsm_set_set().find(wsm->getTimestamp()) != my_rsu_class_00->getWsm_set_set().end();

       my_rsu_class_00->setWsm_set(wsm->getTimestamp());        /*將時間戳存入set清單*/

       if (simTime() - wsm->getTimestamp() < 30 && wsm->getTimestamp() == *my_rsu_class_00->getWsm_set_set().rbegin())
       {
           EV << "reset timer." << endl;
           my_rsu_class_00->setReport_timer(simTime());     /*新進訊息是沒收過的訊息且並未超時*/
       }

       if (wsm->getTimestamp() == *my_rsu_class_00->getWsm_set_set().rbegin())
       {
           my_rsu_class_00->setTarget_pos(wsm->getTargetPos());
       }

   }
   else if (wsm->getPsid() == 30)
   {
       my_rsu_class_00->setReport_timer(simTime());
       my_rsu_class_00->setTarget_pos(wsm->getTargetPos());
       WaveShortMessage* reply01 = new WaveShortMessage("OK");
       populateWSM(reply01);
       reply01->setPsid(0);
       reply01->setSenderPos(check_and_cast<BaseMobility*>(getSimulation()->getModuleByPath("rsu[0].mobility"))->getCurrentPosition());

   }
   else if (wsm->getPsid() == 200)
    {
        WaveShortMessage* reply = new WaveShortMessage("OK");
        populateWSM(reply);
        reply->setSenderAddress(getParentModule()->getIndex());
        reply->setWsmVersion(wsm->getWsmVersion());
        reply->setSenderAddress(getParentModule()->getIndex());
        reply->setPsid(0);
        reply->setRecipientAddress(wsm->getSenderAddress());
        reply->setWsmData("from RSU.");
        EV << "test from RSU. " << traci->junction("53115970").getPosition() << endl;
        sendDown(reply);
    }

}

void MyRSU::onWSA(WaveServiceAdvertisment* wsa)
{

}
