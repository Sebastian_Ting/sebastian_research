//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/modules/application/traci/reportMessage.h>
#include "veins/modules/messages/WaveShortMessage_m.h"
#include "veins/modules/mobility/traci/TraCIScenarioManager.h"
#include "veins/modules/mobility/traci/TraCICommandInterface.h"
#include "veins/modules/mobility/traci/TraCIConnection.h"
#include <string>
#include <iostream>

using namespace std;

Define_Module(reportMessage);

rm_class* my_rm_class = new rm_class;

void reportMessage::initialize(int stage)
{
    BaseWaveApplLayer::initialize(stage);

    if (stage == 0)
    {
        EV << "stage = 0 " << endl;
        EV << "路口座標點測試 " << traci->junction("53115970").getPosition() << endl;
        EV << traci->getJunctionIds().front() << endl;
        string id_x = traci->getJunctionIds().front();
        EV << id_x << endl;
        EV << traci->junction(id_x).getPosition() << endl;
        t_pos.x = 227.02;
        t_pos.y = 458.4;
        t_pos.z = 3;

        my_rm_class->setTtt(0);
        v_test.setName("v_test001");



    }
    else if (stage == 1)
    {
        EV << "stage = 1" << endl;

        just_for_test.insert(10);
        just_for_test.insert(20);
        just_for_test.insert(30);
        just_for_test.insert(40);

        temp_bool = just_for_test.find(20) != just_for_test.end();
        EV << "20是否存在於清單 " << temp_bool << endl;
        temp_bool = just_for_test.find(25) != just_for_test.end();
        EV << "25是否存在於清單 " << temp_bool << endl;

        EV << "模組名稱是 " << getParentModule() << endl;
        EV << "位置是 " << TraCIMobilityAccess().get(getParentModule())->getCurrentPosition();
    }
    else if (stage == 3)
    {
        EV << "stage = 3" << endl;

    }
}

void reportMessage::finish()
{

}

void reportMessage::onBSM(BasicSafetyMessage* bsm)
{

}

void reportMessage::handleSelfMsg(cMessage* msg)
{
    if (WaveShortMessage* wsm = dynamic_cast<WaveShortMessage*>(msg))
    {
        EV << "此時位於handleSelfMsg的訊息是 " << wsm << endl;
        if (wsm->getSerial() < 3 && wsm->getPsid() != 22)
        {
            WaveShortMessage* none003 = wsm->dup();
            none003->setSerial(wsm->getSerial() +1);
            EV << "handleSelfMsg內實際送出的記憶體位址 " << &wsm << endl;
            my_msg_event = cSimulation::getActiveSimulation()->getEventNumber();
            EV << "它的事件編號是 " << my_msg_event << endl;
            eventnameis = cSimulation::getActiveSimulation()->getFullName();
            EV << "它的事件名稱是 " << eventnameis << endl;
            sendDown(wsm);
        }
        else if (wsm->getPsid() == 22)     /*psid_22是需常是被刪除的訊息編號*//*Try to delete the message.*/
        {
            EV << "刪除訊息於handleSelfMessage " << &wsm << endl;
            EV << "被handleSelfMessage刪除的訊息為" << wsm << endl;
            EV << "位於getFES_01中的事件是 " << cSimulation::getActiveSimulation()->getFES()->get(01) << endl;
            EV << "位於getFES_02中的事件是 " << cSimulation::getActiveSimulation()->getFES()->get(02) << endl;
            EV << "位於getFES_03中的事件是 " << cSimulation::getActiveSimulation()->getFES()->get(03) << endl;
            EV << "位於getFES_04中的事件是 " << cSimulation::getActiveSimulation()->getFES()->get(04) << endl;
            EV << "位於getFES_05中的事件是 " << cSimulation::getActiveSimulation()->getFES()->get(05) << endl;
            EV << "目前事件集合的總長度是 " << cSimulation::getActiveSimulation()->getFES()->getLength() << endl;

            cSimulation::getActiveSimulation()->getFES();
            cancelEvent(wsm);
        }

        else if (wsm->getSerial() >= 3 && wsm->getPsid() != 22) {        /*Serial會影響到要傳送幾次！*/
            //stop service advertisements
            stopService();
            delete(wsm);
        }
        else {
            scheduleAt(simTime() + 1, wsm);
        }
    }
    else {
        BaseWaveApplLayer::handleSelfMsg(msg);
    }


}



void reportMessage::onWSM(WaveShortMessage* wsm)
{
    if (getParentModule()->getIndex() != 38)
    switch (wsm->getPsid())
    {
        case 2000:
        {
            distance_s = (1 - traci->getDistance(mobility->getCurrentPosition(), wsm->getSenderPos(), 0)/100) * wait_t;
            forward_t = distance_s;
            forward_t = fabs(forward_t);

            WaveShortMessage* re_nothing = new WaveShortMessage("RE:nothing");
            populateWSM(re_nothing);
            re_nothing->setSenderAddress(wsm->getSenderAddress());
            re_nothing->setTimestamp(wsm->getTimestamp());
            wsm->setWsmData("The first message");
            re_nothing->setSerial(2);
            re_nothing->setPsid(20);


            /*如果住列為空，則存入。否則對比內容*/
            if (wsm->getForward_count() < 1)
            {
                EV << "此訊息尚未被轉傳過，即將轉傳" << endl;
                EV << "我的前進方向是 " << mobility->getAngleRad() << endl;
                re_nothing->setSenderAddress(getParentModule()->getIndex());
                re_nothing->setForward_count(re_nothing->getForward_count() + 1);
                re_nothing->setWsmData("forwarded");
                re_nothing->setForward_from(getParentModule()->getIndex());
                EV << "轉傳訊息之記憶體位址 " << &re_nothing << endl;
                scheduleAt(simTime() + forward_t, re_nothing);
                EV << "已排定訊息行程 " << re_nothing << endl;

            }
            if (wsm->getForward_count() >= 1)      /*如果該事件轉傳次數大於1，則必須確認柱列中是否有相同事件*/
            {
                EV << "檢查訊息(被呼叫於onWSM) " << &re_nothing << endl;
                EV << "我的前進方向是 " << mobility->getAngleRad() << endl;
                simtime_t the_timestamp = wsm->getTimestamp();

                for (int id_x = 0; id_x <= cSimulation::getActiveSimulation()->getFES()->getLength() - 1; id_x++)   /*在未來事件集合中尋找目標事件*/
                {
                    if (cSimulation::getActiveSimulation()->getFES()->get(id_x)->isMessage() == 1)  /*如果某事件屬於訊息事件*/
                    {
                        if (WaveShortMessage* wsm_111 = dynamic_cast<WaveShortMessage*>(cSimulation::getActiveSimulation()->getFES()->get(id_x)))   /*將事件轉換為WSM*/
                        {
                            if (wsm_111->getForward_from() == getParentModule()->getIndex() && wsm_111->getTimestamp() == the_timestamp)    /*如果該事件屬於自己的*/
                            {
                                EV << "已轉換成功進入判斷式" << endl;
                                EV << "讀取集合的車輛模組為 " << getParentModule()->getIndex() << endl;
                                EV << "FES中序號 " << id_x << "中的訊息是來自 " << wsm_111->getForward_from() << endl;
                                cSimulation::getActiveSimulation()->getFES()->remove(cSimulation::getActiveSimulation()->getFES()->get(id_x));
                                break;
                            }
                        }
                    }
                }

            }
        }


    }
}

void reportMessage::handlePositionUpdate(cObject* obj)
{
    BaseWaveApplLayer::handlePositionUpdate(obj);

    traci->junction("53115970").getPosition();

    if (my_rm_class->getId_store_empty()) v_test.record(0);
    else v_test.record(1);



    if (simTime() > ready_to_send && getParentModule()->getIndex() == 9)
    {
        my_rm_class->setId_sotre(9);
        EV << "我的物件是 " << obj->getFullName() << endl;   /*obj_fullName: veinsmobility*/
        msg_s = 1;
        WaveShortMessage* none_msg = new WaveShortMessage("nothing");
        populateWSM(none_msg);
        none_msg->setMy_direction(mobility->getAngleRad());
        none_msg->setSenderAddress(getParentModule()->getIndex());
        none_msg->setPsid(20);     /*訊息種類*/
        none_msg->setWsmVersion(0);
        none_msg->setForward_count(0);
        none_msg->setSerial(2);   /*傳送3次*/
        none_msg->setTargetPos(t_pos);
        EV << "記憶體位址" << &none_msg << endl;
        scheduleAt(simTime(), none_msg);
        ready_to_send = simTime() + 2;
        EV << "最初的事件編號是 " << cSimulation::getActiveSimulation()->getEventNumber() << endl;
        EV << "getParentModule()->getName(): " << getParentModule()->getName() << getParentModule()->getIndex() << endl;    /*抓取節點名稱及應用層編號*/
        EV << "TraCIMobilityAccess().get(getParentModule())的內容是 " << TraCIMobilityAccess().get(getParentModule())->getDisplayString() << endl;
        EV << "路口座標點測試 " << traci->junction("53115970").getPosition() << endl;
        coord_traci.x = 202.02;
        coord_traci.y = 1183.01;

        //coord_trans->traci2omnet(coord_traci);
        //double x1 = traci_connection->traci2omnet(coord_traci).x;
        //traci2omnet(traci_01->junction("53115970").getPosition());
    }
    else if (getParentModule()->getIndex() == 1) EV << "1號車的行進方向為 " << mobility->getAngleRad() << endl;
    else if (getParentModule()->getIndex() == 10)
    {
        angle_calculate = mobility->getAngleRad() + 2 * M_PI;
        EV << "10號車的行進方向為 " << angle_calculate << "原始方向為 " << mobility->getAngleRad() << endl;     /*如果小於0則進行計算*/
        EV << "界線為 " << angle_calculate + M_PI/2 << "以及 " << angle_calculate - M_PI/2 << endl;
    }
}
